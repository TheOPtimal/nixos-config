# NixOS-Config - NixOS Config for TheOPtimal (Jacob Gogichaishvili)
# Copyright (C) 2021  Jacob Gogichaishvili
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
{...}: {
  imports = [
    ../modules/adb.nix
    ../modules/containers.nix
    ../modules/doas.nix
    ../modules/flatpak.nix
    ../modules/fwupd.nix
    ../modules/gnome.nix
    ../modules/gnupg.nix
    ../modules/input.nix
    ../modules/kernel.nix
    ../modules/locale.nix
    ../modules/networking/common.nix
    ../modules/nix.nix
    ../modules/nixpkgs.nix
    ../modules/openrazer.nix
    ../modules/plymouth.nix
    ../modules/printing/client.nix
    ../modules/sound.nix
    ../modules/ssh.nix
    ../modules/sysrq.nix
    ../modules/tailscale.nix
    ../modules/timezone.nix
    ../modules/users.nix

    ../modules/packages/base.nix
    ../modules/packages/desktop.nix
  ];
}
