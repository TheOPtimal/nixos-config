# NixOS Configuration

[![Built with Nix](https://img.shields.io/badge/Built%20With-Nix-blue?style=for-the-badge&logo=nixos&logoColor=9cf)](https://builtwithnix.org/) ![Compatible with HP Spectre x360](https://img.shields.io/badge/Compatible-HP%20Spectre%20x360-blue?color=red&style=for-the-badge) ![Compatible with Lenovo IdeaCentre PC](https://img.shields.io/badge/Compatible-Lenovo%20IdeaCentre%20PC-blue?color=red&style=for-the-badge) [![Fuck it - Ship it](https://forthebadge.com/images/badges/fuck-it-ship-it.svg)](https://forthebadge.com) [![Uses badges](https://forthebadge.com/images/badges/uses-badges.svg)](https://forthebadge.com) [![60% of the time - Works every time](https://forthebadge.com/images/badges/60-percent-of-the-time-works-every-time.svg)](https://forthebadge.com)

This repository contains NixOS configurations for TheOPtimal's servers and computers

## Installation

```bash
git clone https://gitlab.com/TheOPtimal/nixos-config NixOS-Config
cd NixOS-Config
sudo nixos-rebuild --flake . --impure
```

## Hacking

To hack on the code, either enter the dev shell

```bash
nix develop
```

Or use direnv

```bash
direnv allow
```
