# NixOS-Config - NixOS Config for TheOPtimal (Jacob Gogichaishvili)
# Copyright (C) 2021  Jacob Gogichaishvili
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
{...}: {
  imports = [
    ../avahi/server.nix # Avahi server to make print server discoverable
    ../printing/client.nix # Import client configuration as not to have to redefine some stuff
  ];

  services.printing.webInterface = true; # Re-enable web interface because it was disabled in the client
  services.printing.browsing = true;
  services.printing.listenAddresses = ["*:631"]; # Not 100% sure this is needed and you might want to restrict to the local network
  services.printing.allowFrom = ["all"]; # this gives access to anyone on the interface you might want to limit it see the official documentation
  services.printing.defaultShared = true; # If you want

  # Expose print server on firewall
  networking.firewall.allowedUDPPorts = [631];
  networking.firewall.allowedTCPPorts = [631];
}
