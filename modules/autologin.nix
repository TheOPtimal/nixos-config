_: {
  # Automatically log in to user on startup in graphical environments
  services.xserver.displayManager.autoLogin = {
    enable = true;
    user = "optimal";
  };
  # Automatically log in to user on startup in text-based environments
  services.getty.autologinUser = "optimal";
}
