_: {
  # Hardware
  hardware = {
    enableAllFirmware = true;
    enableRedistributableFirmware = true;
  };
}
