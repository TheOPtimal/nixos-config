_: {
  # Allow sysrq for things like REISUB to work
  boot.kernel.sysctl."kernel.sysrq" = 1;
}
