_: {
  # Enable NixOS Containers
  boot.enableContainers = true;

  # Bridge networking
  networking.nat.enable = true;
  networking.nat.internalInterfaces = ["ve-+"];

  # Prevent NetworkManager from managing the bridge
  networking.networkmanager.unmanaged = ["interface-name:ve-*"];
}
