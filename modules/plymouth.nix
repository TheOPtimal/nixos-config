_: {
  boot = {
    plymouth.enable = true;

    kernelParams = ["quiet"]; # Less text on boot
  };
}
