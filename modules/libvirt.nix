# NixOS-Config - NixOS Config for TheOPtimal (Jacob Gogichaishvili)
# Copyright (C) 2021  Jacob Gogichaishvili
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
{
  config,
  pkgs,
  lib,
  ...
}: {
  virtualisation = {
    libvirtd = {
      enable = true;
      qemu = {
        ovmf.packages = [pkgs.OVMFFull.fd pkgs.pkgsCross.aarch64-multiplatform.OVMF.fd];
        swtpm.enable = true;
      };
    };
  };

  environment.systemPackages = lib.optionals config.services.xserver.enable (with pkgs; [
    virt-manager
  ]);

  # NixOps support
  users.users.optimal.extraGroups = ["libvirtd"];
  networking.firewall.checkReversePath = false;
}
