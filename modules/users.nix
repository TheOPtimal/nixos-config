# NixOS-Config - NixOS Config for TheOPtimal (Jacob Gogichaishvili)
# Copyright (C) 2021  Jacob Gogichaishvili
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
{pkgs, ...}: {
  programs.zsh.enable = true;

  users.mutableUsers = false; # This dissallows the changing of user info like password and groups.
  users.users.optimal = {
    isNormalUser = true;
    description = "Jacob Gogichaishvili"; # Description usually refers to full name
    hashedPassword = "$6$CWykZ29hM.4bu01X$1nMDWPzQXT0aAGJDKfLwG.2jZxy.wRE1U9GC6m8KSthL/IWB2QgoVpReluRA8IBbU7Dgj5dIiuaVxTZklUmZS.";
    extraGroups = ["wheel"]; # Wheel enables ‘sudo’ for the user.
    shell = pkgs.zsh;
    # Add SSH key(s)
    openssh.authorizedKeys.keys = [
      "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDJh+uYX3r0lqqyuKrc3/cSAubL9xcgBJp0e3Acx+/YF2SdOPV7A+4XVBRtGTMGmRPEojuwxd5BJETxqDFO115gth1jhcJEipT7FAsBFxLmKXX5zVWY87Evt+PwE7qO2EfqDHJE8TQnAL6wfJ5C6LmYXGmZ3nmX2G1vRnkUxEIAOP7n8LO6OCCPnmgKzv/zdvk3ztBYUr7yr9RPDqICZXhS382ZzK5GNetuhOn5/lM4/S9bjwdusOKCrJ9l+ecEHqcbE0d6unIEic0wbErbOUbJauzNNQDsUiK2+gDJHmwq59xJMjui/V8GQAKEObXAP/CfbIxX0sFsbMGSvlRu+BGh TheOPtimal Key"
    ];
  };
}
