# NixOS-Config - NixOS Config for TheOPtimal (Jacob Gogichaishvili)
# Copyright (C) 2021  Jacob Gogichaishvili
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
_: {
  # Disable default sound.
  sound.enable = false;
  hardware.pulseaudio.enable = false;

  # rtkit is optional but recommended
  security.rtkit.enable = true;

  # Enable pipewire - a PulseAudio replacement that's much better.
  services.pipewire = {
    enable = true;

    # Redirect ALSA apps to Pipewire
    alsa = {
      enable = true;
      support32Bit = true;
    };

    # Pulse and JACK emulation
    pulse.enable = true;
    jack.enable = true;

    # Enable WirePlumber instead of pipewire-media-session
    wireplumber.enable = true;
    media-session.enable = false;
  };
}
