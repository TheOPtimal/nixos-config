# NixOS-Config - NixOS Config for TheOPtimal (Jacob Gogichaishvili)
# Copyright (C) 2021  Jacob Gogichaishvili
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
_: {
  networking = {
    networkmanager.enable = true;

    # The global useDHCP flag is deprecated, therefore explicitly set to false here.
    # Per-interface useDHCP will be mandatory in the future, so this generated config
    # replicates the default behaviour.
    useDHCP = false;

    # Configure network proxy if necessary
    # proxy = {
    #   default = "http://user:password@proxy:port/";
    #   noProxy = "127.0.0.1,localhost,internal.domain";
    # };

    nameservers = ["1.1.1.1" "1.0.0.1" "8.8.8.8"];
  };

  # Add user to group to allow them to control NetworkManager
  users.users.optimal.extraGroups = ["networkmanager"];
}
