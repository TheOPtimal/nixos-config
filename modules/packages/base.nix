# NixOS-Config - NixOS Config for TheOPtimal (Jacob Gogichaishvili)
# Copyright (C) 2021  Jacob Gogichaishvili
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
{pkgs, ...}: {
  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
    vim # Do not forget to add an editor to edit configuration.nix! The Nano editor is also installed by default.

    nixos-option

    diffutils
    findutils
    util-linux
    tzdata
    hostname
    man
    gnugrep
    gnused
    bzip2
    gzip
    xz
    zip
    unzip
    wget

    pciutils
    usbutils
    alsa-utils
    killall
  ];
}
