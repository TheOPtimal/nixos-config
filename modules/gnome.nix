# NixOS-Config - NixOS Config for TheOPtimal (Jacob Gogichaishvili)
# Copyright (C) 2021  Jacob Gogichaishvili
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
{
  config,
  pkgs,
  lib,
  ...
}: let
  gsconnectRange = {
    from = 1714;
    to = 1764;
  };
in {
  # Enable the X11 windowing system.
  services.xserver.enable = true;

  # Enable the GNOME Desktop Environment.
  services.xserver.displayManager.gdm.enable = true; # Login manager for GNOME
  services.xserver.desktopManager.gnome.enable = true;

  # GSConnect ports
  networking.firewall.allowedTCPPortRanges = [gsconnectRange];
  networking.firewall.allowedUDPPortRanges = [gsconnectRange];

  # GNOME Network Display Miracast ports
  networking.firewall.allowedTCPPorts = [7236 7250];
  networking.firewall.allowedUDPPorts = [7236 5353];

  environment.systemPackages = with pkgs;
  with pkgs.gnomeExtensions;
  with pkgs.gnome; [
    # Other core GNOME apps
    gnome-sound-recorder
    # Wayland Utilities
    wl-clipboard
    brightnessctl
    # GNOME Extensions
    (lib.mkIf config.hardware.sensor.iio.enable screen-autorotate)
  ];
}
