# NixOS-Config - NixOS Config for TheOPtimal (Jacob Gogichaishvili)
# Copyright (C) 2021  Jacob Gogichaishvili
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
{pkgs, ...}: {
  nix = {
    # Pick stable version of Nix
    package = pkgs.nixVersions.stable;
    # Enable flake support
    extraOptions = ''
      experimental-features = nix-command flakes
    '';
    # Allow the wheel group to do some advanced stuff (for deploy-rs)
    settings.trusted-users = ["root" "@wheel"];
  };
}
