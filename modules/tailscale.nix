{
  pkgs,
  lib,
  ...
}: {
  # Enable Tailscale, an easy mesh networking solution.
  services.tailscale.enable = true;

  # Autostart tailscale
  systemd.services.tailscale-autoconnect = {
    description = "Automatically start up Tailscale";

    # make sure tailscale is running before trying to connect to tailscale
    after = ["network-pre.target" "tailscale.service"];
    wants = ["network-pre.target" "tailscale.service"];
    wantedBy = ["multi-user.target"];

    # set this service as a oneshot job
    serviceConfig.Type = "oneshot";

    # have the job run this shell script
    script = "${pkgs.tailscale}/bin/tailscale up --operator optimal --advertise-exit-node";
  };

  # IP Forwarding
  boot.kernel.sysctl = {
    "net.ipv4.ip_forward" = 1;
    "net.ipv6.conf.all.forwarding" = 1;
  };

  # Configure MagicDNS
  networking.nameservers = lib.mkBefore ["100.100.100.100"];
}
