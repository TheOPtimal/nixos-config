{
  config,
  pkgs,
  lib,
  ...
}: {
  # Enable flatpak
  services.flatpak.enable = true;

  # If GNOME is enabled, install GNOME Software
  environment.systemPackages = lib.mkIf config.services.xserver.desktopManager.gnome.enable (with pkgs; [gnome.gnome-software]);
}
