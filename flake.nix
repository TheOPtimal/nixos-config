# NixOS-Config - NixOS Config for TheOPtimal (Jacob Gogichaishvili)
# Copyright (C) 2021  Jacob Gogichaishvili
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
{
  description = "NixOS configuration";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";

    flake-utils.url = "github:numtide/flake-utils";

    home-manager.url = "github:nix-community/home-manager";
    home-manager.inputs = {
      nixpkgs.follows = "nixpkgs";
      utils.follows = "flake-utils";
    };

    hm-config.url = "gitlab:TheOPtimal/home-manager-config";
    hm-config.inputs = {
      nixpkgs.follows = "nixpkgs";
      home-manager.follows = "home-manager";
      flake-utils.follows = "flake-utils";
    };

    deploy-rs.url = "github:serokell/deploy-rs";
    deploy-rs.inputs.nixpkgs.follows = "nixpkgs";

    nixos-hardware.url = "github:NixOS/nixos-hardware/master";
  };

  outputs = {
    self,
    home-manager,
    hm-config,
    nixpkgs,
    flake-utils,
    deploy-rs,
    ...
  } @ inputs:
    {
      nixosConfigurations = let
        # Boilerplate to create basic desktop config
        mkDesktopConfig = configPath:
          nixpkgs.lib.nixosSystem rec {
            system = "x86_64-linux";
            modules = [
              configPath

              # Out-of-tree modules
              home-manager.nixosModules.home-manager
            ];
            # Special arguments provided to modules
            specialArgs = {
              inherit inputs;
              hm-config = hm-config.nixosModules.${system};
            };
          };
        # Functions to automatically detect configurations in the folder
        mkSystem = hostname: mkDesktopConfig ./configurations/${hostname}/configuration.nix;
        mkSystems = configurations: builtins.mapAttrs (configDir: _: mkSystem configDir) (builtins.readDir configurations);
        # Read `configurations` directory and import and create flake output for each one
      in
        mkSystems ./configurations;

      packages."x86_64-linux".live-iso = self.nixosConfigurations.live-iso.config.system.build.isoImage;

      deploy.nodes = let
        # Generate deploy-rs system profile
        mkSysProfile = config: {
          sshUser = "optimal";
          user = "root";
          path = deploy-rs.lib.x86_64-linux.activate.nixos config;
        };
        # Generate deploy-rs system
        mkSystem = hostname: {
          inherit hostname;
          profiles.system = mkSysProfile self.nixosConfigurations.${hostname};
        };
        # For each NixOS Config, also create deploy-rs profile
      in
        builtins.mapAttrs mkSystem self.nixosConfigurations;

      # Generate checks with deploy-rs
      checks = builtins.mapAttrs (_system: deployLib: deployLib.deployChecks self.deploy) deploy-rs.lib;
    }
    // (flake-utils.lib.eachDefaultSystem (system: let
      pkgs = nixpkgs.legacyPackages.${system};
      deploy-rs-cli = deploy-rs.packages.${system}.deploy-rs;
    in {
      devShell = pkgs.mkShell {
        buildInputs = with pkgs; [
          nixFlakes
          (nixos-rebuild.override {nix = nixFlakes;}) # Make flakes work properly with nixos-rebuild
          deploy-rs-cli
        ];
      };
    }));
}
