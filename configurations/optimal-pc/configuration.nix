# NixOS-Config - NixOS Config for TheOPtimal (Jacob Gogichaishvili)
# Copyright (C) 2021  Jacob Gogichaishvili
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
# Configuration for `optimal-pc`
# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).
{
  inputs,
  hm-config,
  ...
}: {
  imports = with inputs.nixos-hardware.nixosModules; [
    # Include the results of the hardware scan.
    ./hardware-configuration.nix

    hm-config.desktop

    ../../module-collections/powerful-desktop.nix

    ../../modules/networking/optimal-pc.nix
    ../../modules/systemd-boot.nix

    common-pc
    common-pc-hdd
    common-cpu-intel
    common-gpu-nvidia
  ];

  # Disable Nvidia PRIME

  hardware.nvidia.prime.offload.enable = false;

  # Steam issue workaround

  programs.steam.enable = true;

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "21.11"; # Did you read the comment?
}
