# NixOS-Config - NixOS Config for TheOPtimal (Jacob Gogichaishvili)
# Copyright (C) 2021  Jacob Gogichaishvili
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
# Configuration for `optimalbook` (HP Spectre x360 13 aw2xxx)
# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).
{
  modulesPath,
  inputs,
  hm-config,
  lib,
  ...
}: {
  imports = with inputs.nixos-hardware.nixosModules; [
    "${modulesPath}/installer/cd-dvd/installation-cd-base.nix"
    "${modulesPath}/installer/cd-dvd/channel.nix"

    hm-config.desktop-minimal

    ../../module-collections/general-desktop.nix

    ../../modules/sensors.nix
    ../../modules/autologin.nix
  ];

  networking.wireless.enable = lib.mkForce false;

  services.getty = {
    autologinUser = lib.mkForce "optimal";
    helpLine = ''
      Consult the manual (nixos-help) for documentation.
      Consult `man configuration.nix` for system configuration;

      Run `nixos-generate-config --show-hardware-config --root /mnt` (after editing your partitions and mounting them) to get a hardware-configuration.nix
      `git clone https://gitlab.com/TheOPtimal/NixOS-Config ~/NixOS-Config` to clone the configuration and start editing (although ideally you'd want to send off hardware-configuration.nix to somewhere else and start editing there)

      Happy tinkering and installing!
    '';
  };

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "21.11"; # Did you read the comment?
}
